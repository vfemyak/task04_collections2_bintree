public class Node<K,V> {
    private K key;
    private V value;
    private Node<K,V> leftNode;
    private Node<K,V> rightNode;

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public Node<K, V> getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node<K, V> leftNode) {
        this.leftNode = leftNode;
    }

    public Node<K, V> getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node<K, V> rightNode) {
        this.rightNode = rightNode;
    }

    @Override
    public String toString() {
        return "Node [" + "Key = " + this.key + ", Value = " + this.value + "]";
    }
}
