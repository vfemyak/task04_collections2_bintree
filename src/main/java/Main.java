
public class Main {
    public static void main(String[] args) {
        BinaryTree<Integer,String> tree = new BinaryTree<>();

        tree.addNode(5,"five");
        tree.addNode(15,"fifteen");
        tree.addNode(3,"three");
        tree.addNode(1,"one");
        tree.addNode(11,"eleven");

        tree.print(tree.getRoot());

        tree.removeNode(1);

        tree.print(tree.getRoot());

        System.out.println(tree.getNode(15));
    }
}
