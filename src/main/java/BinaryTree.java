public class BinaryTree<K,V> {

    private Node<K, V> root;

    public Node<K, V> getRoot() {
        return root;
    }

    public Node<K, V> getNode(int key) {        //finding element
        Node<K, V> currentNode = root;

        while ((Integer) currentNode.getKey() != key) {
            if (key < (Integer) currentNode.getKey())    //додати try-catch
                currentNode = currentNode.getLeftNode();
            else
                currentNode = currentNode.getRightNode();
            if (currentNode == null)
                return null;
        }
        return currentNode;
    }

    public void addNode (int key, V value){         //як змінити тип key на <K>
        Node newNode = new Node<>();
        newNode.setKey(key);
        newNode.setValue(value);

        if (root == null)
            root = newNode;
        else {
            Node currentNode = root;
            Node parentNode;

            while (true){
                parentNode = currentNode;
                if (key < (Integer) currentNode.getKey()) {         //чи можна якимось чином порівнювати об'єкти
                    currentNode = currentNode.getLeftNode();
                    if (currentNode == null) {
                        parentNode.setLeftNode(newNode);
                        return;
                    }
                }
                else {
                    currentNode = currentNode.getRightNode();
                    if (currentNode == null) {
                        parentNode.setRightNode(newNode);
                        return;
                    }
                }
            }
        }
    }

    public void print (Node curreNode){
        if (curreNode != null){
            print(curreNode.getLeftNode());
            System.out.println(curreNode.getKey() + " ");
            print(curreNode.getRightNode());
        }
    }

    public boolean removeNode (int key) {
        Node currentNode = root;
        Node parrent = root;
        boolean isLeftNode = true;

        while ((Integer) currentNode.getKey() != key) {
            parrent = currentNode;
            if (key < (Integer) currentNode.getKey()) {
                isLeftNode = true;
                currentNode = currentNode.getLeftNode();
            } else {
                isLeftNode = false;
                currentNode = currentNode.getRightNode();
            }
            if (currentNode == null)
                return false;
        }

        if (currentNode.getLeftNode() == null && currentNode.getRightNode() == null) {  //листок
            if (currentNode == root)
                root = null;
            else if (isLeftNode)
                parrent.setLeftNode(null);
            else
                parrent.setRightNode(null);
        } else if (currentNode.getRightNode() == null) {
            if (currentNode == root)
                root = currentNode.getLeftNode();
            else if (isLeftNode)
                parrent.setLeftNode(currentNode.getLeftNode());
            else
                parrent.setRightNode(currentNode.getLeftNode());
        } else if (currentNode.getLeftNode() == null) {
            if (currentNode == root)
                root = currentNode.getRightNode();
            else if (isLeftNode)
                parrent.setLeftNode(currentNode.getRightNode());
            else
                parrent.setRightNode(currentNode.getRightNode());
        } else {
            Node successor = getSuccessor(currentNode);
            if (currentNode == root)
                root = successor;
            else if (isLeftNode)
                parrent.setLeftNode(successor);
            else
                parrent.setRightNode(successor);
            successor.setLeftNode(currentNode.getLeftNode());
        }
        return true;
    }

    private Node getSuccessor (Node delNode){
        Node successorParent = delNode;
        Node successor = delNode;
        Node current = delNode.getRightNode();
        while (current != null){
            successorParent = successor;
            successor = current;
            current = current.getLeftNode();
        }

        if (successor != delNode.getRightNode()){
            successorParent.setLeftNode(successor.getRightNode());
            successor.setRightNode(delNode.getRightNode());
        }
        return successor;
    }
}
